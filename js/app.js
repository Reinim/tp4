/*app.js 
Fichier de fonctions jquery pour le TP Pizza
Codé par Minier Théo
OCRES TD 2
2019
*/

//Variables utilisées pour le calcul du nombre de parts de pizza à affiché
window.nbpizz = 0;
window.ancien = 0;

/*
Fonction affichant les ingrédients des pizzas au survol du curseur
*/
$(function(){$('.pizza-type').find('label').hover(function(){

    $(this).find('.no-display').show();
},function(){

    $(this).find('.no-display').hide();
} );

            });


/*
Fonction affichant le nombre de parts de pizza (après avoir appoyé sur enter dans le champs nombre de part)
*/
$(function(){$('.nb-parts').children('input').change(function() 
                                                     {
    if (window.nbpizz===0) //Au chargement de la page, vérifie que c'est le premier affichage modifié
    {
        $('.nb-parts').children('.pizza-pict').toggleClass('pizza-1',false);
        window.nbpizz = 'pizza-' + $('.nb-parts').children('input').val(); //donne le nom de la classe qui sera la précédente par la suite
    }

    if(window.ancien!=0) //efface les anciennes pizzas affichées (si supérieur à 1)
    {
        for(var i=0; i<window.ancien; i++)
        {
            $('.nb-parts').children('.pizza-pict').next().remove();
        }
    }

    if($('.nb-parts').children('input').val()<7)//si on demande moins de 7 parts
    {
        var nbpiz = 'pizza-' + $('.nb-parts').children('input').val(); //donne le nom de la classe actuelle
    }

    $('.nb-parts').children('.pizza-pict').toggleClass(window.nbpizz,false);    //supprime la classe précédente

    if($('.nb-parts').children('input').val()>6)
    {
        var NombreDePizzas = Math.floor($('.nb-parts').children('input').val()/6)
        window.ancien = NombreDePizzas;
        console.log('nb de pizza pleine ' + NombreDePizzas);
        for(var i=0; i<NombreDePizzas; i++)
        {
            $('.nb-parts').children('.pizza-pict').first().clone().toggleClass('pizza-6').appendTo('.nb-parts');
            //cloner pizza pleine
        }

        var nbpiz = 'pizza-' + ($('.nb-parts').children('input').val() %6);
        $('.nb-parts').children('.pizza-pict').toggleClass(nbpiz,true);  //ajoute la classe actuelle
        window.nbpizz = 'pizza-' + ($('.nb-parts').children('input').val() %6);
        console.log('nb de parts ' + window.nbpizz);


    }


    if($('.nb-parts').children('input').val()<7)//si on demande moins de 7 parts
    {
        $('.nb-parts').children('.pizza-pict').toggleClass(nbpiz,true);  //ajoute la classe actuelle
        window.nbpizz = 'pizza-' + $('.nb-parts').children('input').val();          //sauvegarde la classe actuelle qui deviendra précédente
    }

});

            });

/*
Fonction affichant les informations à personelles à faire entrer par le client après un clic
sur le bouton de validation
*/
$(function(){$(".btn-success").click(function() {
    $(".infos-client").show();
    $(this).hide();
});
            });

/*
Fonction ajoutant un champ addresse pour l'utilisateur après clic
*/
$(function(){$(".add").click(function() {
    // $('.infos-client').children().next().find('input').first().clone().removeClass().appendTo($(".infos-client").children().next().first().children().first());
    //Doit changer la taille et l'emplacement du champ texte


    $(this).parent().prepend('<input style="display: block" type="text"/>')
    //C'est bon

    //Penser à blinder si + de 2 clics
});
            });



function calculDePanier() 
{
    
    var tarif = $('.pizza-type').find(':radio:checked').data('price'); //prix d'une pizza
    var nbParts = $('.nb-parts').children('input').val();//nb de parts de pizza
    var prixPate = $('.pizza-type').next().next().find('label').find(':radio:checked').data('price'); //prix de la pate
    var prixExtra = 0;

    //prix des extras
    if($('.main').find('label').find(':checkbox').prop('checked'))
        {
            prixExtra += $('.main').find('label').find(':checkbox').data('price');
        }
    
    if($('.main').find('label').next().find(':checkbox').prop('checked'))
        {
            prixExtra += $('.main').find('label').next().find(':checkbox').data('price');
        }
    if($('.main').find('label').next().next().find(':checkbox').prop('checked'))
        {
            prixExtra += $('.main').find('label').next().next().find(':checkbox').data('price');
        }
    if($('.main').find('label').next().next().next().find(':checkbox').prop('checked'))
        {
            prixExtra += $('.main').find('label').next().next().next().find(':checkbox').data('price');
        }

//calcul total avec 2 decimal
    var Total = ((nbParts/6)*(tarif+prixPate+prixExtra)).toFixed(2);

    $('.tile-title').next().text(""+Total);
    
    
}

/*
Fonction affichant un message de remerciement en supprimant tous les éléments de page
*/
$(function(){$(".done").click(function() {
    var prenom = $('.infos-client').children().children('input').val();

    var ligne = "<p>Merci " + prenom + " ! Votre commande sera livrée dans 15 minutes !</p>";

    calculDePanier();
     $('.container').empty();
    $( ".container" ).append( ligne );
});
            });


/*Penser à ajouter des required aux champs*/

/*Penser à commenter*/


/*
Question bonus:

Faire une nouvelle branche dans votre repository que l'on appelera "bonus". Dans le cadre de cette question bonus, vous avez le droit de modifier le html

1 - S'il fait moins de 0 degrès celsius augmenter le prix de 5 euros pour le tips du livreur.
*/
